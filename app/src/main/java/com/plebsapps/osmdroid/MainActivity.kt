package com.plebsapps.osmdroid

import android.Manifest
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import org.osmdroid.config.Configuration
import org.osmdroid.util.GeoPoint
import org.osmdroid.api.IMapController


class MainActivity : AppCompatActivity() {

    public override fun onCreate(savedInstance: Bundle?) {
        super.onCreate(savedInstance)
        Configuration.getInstance().load(applicationContext, PreferenceManager.getDefaultSharedPreferences(applicationContext))
        setContentView(R.layout.activity_main)

        requestPermission()
        setZoomAndLocation()
    }

    private fun setZoomAndLocation(){
        mapView.controller.setZoom(15.0)
        mapView.controller.setCenter(GeoPoint(50.806244, 8.884305))
    }

    private fun requestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE), Const.AskPermissionRequestCode)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            Const.AskPermissionRequestCode -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.d(Const.TAG, "Permission has been denied by user")
                } else {
                    Log.d(Const.TAG, "Permission has been granted by user")
                }
            }
        }
    }

    public override fun onResume() {
        super.onResume()
        Configuration.getInstance().load(applicationContext, PreferenceManager.getDefaultSharedPreferences(applicationContext))
        if (mapView != null)
            mapView!!.onResume()
    }

    public override fun onPause() {
        super.onPause()
        Configuration.getInstance().save(applicationContext, PreferenceManager.getDefaultSharedPreferences(applicationContext))
        if (mapView != null)
            mapView!!.onPause()
    }
}